<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jogo da velha!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">

	
	<SCRIPT LANGUAGE="javascript" TYPE="text/javascript">
		function jogar(){
			
			menu=document.getElementById("menu");
			document.getElementById("menu").style.display = "none";
			document.getElementById("formulario").style.display="inline";
		}
		function conectar(){
			document.getElementById("formulario").style.display="none";	
			document.getElementById("campoJogo").style.display="inline";
		}
		
	</SCRIPT>

</head>
<body>
	

	<div class="container-fluid" id="menu">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-5">
				</div>
				<div class="col-md-2">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h1 class="panel-title" align=center>
								JOGO DA VELHA 
							</h1>
						</div>
						<div class="panel-body opcoes" id="jogar" onclick="jogar()" align=center>
							Jogar
						</div>
						<div class="panel-footer opcoes" align=center>
							Sair
						</div>
					</div>
				</div>
				<div class="col-md-5">
				</div>
			</div>
		</div>
	</div>
</div>




<div class="container-fluid"  style="display:none;" id="formulario">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<form role="form">
						<div class="form-group">
							 
							<label for="exampleInputEmail1">
								<h3> Digite o IP do servidor</h3>
							</label>
							<input type="text" class="form-control" id=""placeholder="000.000.000.000" />
						</div>
						<div class="form-group">
							 
							<label for="">
								<h3>Digite a porta do servidor</h3>
							</label>
							<input type="text" class="form-control" id="" placeholder="000"/>
						</div>
						
					</form>
					<button type="" class="btn btn-default" onclick="conectar()">
							Conectar
					</button>
				</div>
				<div class="col-md-4">
				</div>
			</div>
		</div>
	</div>
</div>


 <table border="1" id="campoJogo" style="display:none;" >
 <caption><h2 align=center>JOGO DA VELHA</h2></caption>
   <tr ><td class="campo" align=center>O</td> <td class="campo"align=center>X</td> <td class="campo"align=center>O</td> </tr>
   <tr ><td class="campo"align=center>X</td> <td class="campo"align=center>O</td> <td class="campo"align=center>X</td> </tr>
   <tr ><td class="campo"align=center>O</td> <td class="campo"align=center>X</td> <td class="campo"align=center>X</td> </tr>
   <tr style="border: none;"><td style="border: none;">
   <form>
   		<button type="submit" class="btn btn-default" onclick>
			Sair
		</button>
	</form>
	</td></tr>
  	 
</table>

<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</body>
</html>