#!/usr/bin/env python
# -*- coding: utf-8 -*-
import platform
import os
import socket
import sys

#Testado em ambiente Windows Python 3.4.4
#Testado em ambiente Linux Python 2.7
# Para uma melhor compatibilidade ao testar o cliente o servidor deve enviar os dados para o cliente python
# atraves de writeUTF ao inves de writebytes

HOST = ""
PORT = ""
tcp = ""
dest = ""
def ConectaServidor():
    print('Digite o IP do servidor (000.000.000.000): ')
    HOST = sys.stdin.readline().encode()
    print('Digite a porta do servidor (0000): ')
    PORT = sys.stdin.readline().encode()
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        dest = (HOST, int(PORT))
    except (ValueError):
        print("Nº da porta incorreto, tente um valor entre 0 e 65535")
    else:
        try:
            tcp.connect(dest)
        except (ConnectionRefusedError, ValueError, OverflowError, OSError):
            print("\nFalha na Conexão, Verifique se o Servidor está on-line ou se os dados estão corretos \n")
        else:
           msg = tcp.recv(4096).decode()
           sys.stdout.write(msg);
           sys.stdout.flush()
           emsg = sys.stdin.readline().encode()
           while True:
                try:
                        tcp.sendall(emsg)
                        quant_recebida = 0
                        quant_esperada = len(emsg)
                        while quant_recebida < quant_esperada:
                            msg = tcp.recv(4096).decode()
                            quant_recebida += len(msg)
                        print(msg)
                        emsg = sys.stdin.readline().encode()  #obs: a partir desse ponto necessario teclar "enter" 2 vezes para receber resposta do servidor"
                except ConnectionAbortedError:
                       break
                except ConnectionResetError:
                       tcp.close()
                       break
def Limpa():
    tiposo = platform.system()
    if tiposo == 'Windows':
        os.system("cls")
    if tiposo == 'Linux':
        os.system("clear")

def main():
    while True:
        print("---- Jogo da Velha ----")
        print(" 1 - Entrar no Jogo.")
        print(" 0 - Sair.          ")
        print("-----------------------")
        opcao = input("Digite o número da opção: ")
        if opcao == "1":    #remover as aspas para correta execução no Linux
            ConectaServidor()
        elif opcao == "0":  #remover as aspas para correta execução no Linux
            break
        else:
            Limpa()

if __name__ == '__main__':
    main()

