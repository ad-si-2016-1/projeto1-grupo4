from tkinter import *
import socket
import tkinter.messagebox
from pip._vendor.distlib.compat import raw_input

# Cliente com Interface Grafica naão funcional fora do Escopo do Projeto).
# Servidor trabalha apenas com linha de comando.
try:
	#py 2.x
	from Tkinter import *
except ImportError:
	#py 3.x
	from tkinter import *

HOST = ""     # Endereco IP do Servidor
PORT = ""           # Porta do Servidor
tcp = ""
bclick = True
buttons = ""

class Application:
      def __init__(self, master=None):
          self.fonte = ("Verdana", "8")

          self.container2 = Frame(master)
          self.container2["padx"] = 20
          self.container2["pady"] = 5
          self.container2.pack()
          self.container3 = Frame(master)
          self.container3["padx"] = 20
          self.container3["pady"] = 5
          self.container3.pack()
          self.container4 = Frame(master)
          self.container4["padx"] = 20
          self.container4["pady"] = 10
          self.container4.pack()

          self.container5 = Frame(master)
          self.container5["padx"] = 20
          self.container5["pady"] = 5
          self.container5.pack()
          self.container6 = Frame(master)
          self.container6["padx"] = 20
          self.container6["pady"] = 5
          self.container6.pack()
          self.container7 = Frame(master)
          self.container7["padx"] = 20
          self.container7["pady"] = 5
          self.container7.pack()
          self.container8 = Frame(master)
          self.container8["padx"] = 20
          self.container8["pady"] = 10
          self.container8.pack()
          self.container9 = Frame(master)
          self.container9["pady"] = 15
          self.container9.pack()

          self.lbservidor = Label(self.container2, text="Servidor:", font=self.fonte, width=10)
          self.lbservidor.pack(side=LEFT)

          self.txtservidor = Entry(self.container2)
          self.txtservidor["width"] = 20
          self.txtservidor["font"] = self.fonte
          self.txtservidor.pack(side=LEFT)

          self.lbporta = Label(self.container2, text="Porta:", font=self.fonte, width=7)
          self.lbporta.pack(side=LEFT)

          self.txtporta = Entry(self.container2)
          self.txtporta["width"] = 10
          self.txtporta["font"] = self.fonte
          self.txtporta.pack(side=RIGHT)

          self.btnconecta = Button(self.container4, text="Conectar", font=self.fonte, width=12)
          self.btnconecta["command"] = self.conectaservidor
          self.btnconecta.pack (side=LEFT)

          self.btndisconecta = Button(self.container4, text="Disconectar", font=self.fonte, width=12)
          self.btndisconecta["command"] = self.desconectaservidor
          self.btndisconecta.pack (side=LEFT)

          #self.lblmsg = Label(self.container9, text="")
          #self.lblmsg["font"] = ("Verdana", "9", "italic")
          #self.lblmsg.pack()
          global buttons
          buttons= StringVar()

          def marcajogada(buttons):
              global bclick
              if buttons["text"] == " " and bclick == True:
                  buttons["text"] = "X"
                  bclick = False
              elif buttons["text"] == " " and bclick == False:
                  buttons["text"] = "O"
                  bclick = True

          self.button1 = Button(self.container5, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button1))
          self.button1.pack(side=LEFT)
          self.button2 = Button(self.container5, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button2))
          self.button2.pack(side=LEFT)
          self.button3 = Button(self.container5, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button3))
          self.button3.pack(side=LEFT)
          self.button4 = Button(self.container6, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button4))
          self.button4.pack(side=LEFT)
          self.button5 = Button(self.container6, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button5))
          self.button5.pack(side=LEFT)
          self.button6 = Button(self.container6, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button6))
          self.button6.pack(side=LEFT)
          self.button7 = Button(self.container7, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button7))
          self.button7.pack(side=LEFT)
          self.button8 = Button(self.container7, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button8))
          self.button8.pack(side=LEFT)
          self.button9 = Button(self.container7, text=" ", font=('Arial 30 bold'), height=2, width=4, command=lambda:marcajogada(self.button9))
          self.button9.pack(side=LEFT)


      def conectaservidor(self):
          try:
              HOST = self.txtservidor.get()
              PORT = self.txtporta.get()
              tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
              dest = (HOST, int(PORT))
              try:
                  tcp.connect(dest)
              except ConnectionRefusedError:
                  tkinter.messagebox.showinfo("Erro de Conexão", "Verifique se o Servidor está on-line")
              while True:
                tcp.sendall(msg)
          except (ValueError,  OverflowError, OSError):
              tkinter.messagebox.showinfo("Erro de Conexão", "Insira Servidor e Porta, válidos para Conexão")

      def desconectaservidor(self):
          tcp.close()


root = Tk()
Application(root)
root.mainloop()