package br.ufg.inf.servidor;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

public class ClienteThread extends Thread{
	Servidor servidor;
	private static ArrayList<ClienteThread> clienteThread;
	private static ArrayList<ClienteThread> filaThread;

	Socket socketCliente;
	String nomeJogador;
	char caractereJogo;

	public BufferedReader doCliente;
	public DataOutputStream paraCliente;
	private Regra regra;
	private Visao visao;
	
	public ClienteThread(Socket socketCliente, Servidor servidor){
		try{
			this.servidor = servidor;
			this.socketCliente = socketCliente;

			doCliente = new BufferedReader(new InputStreamReader(socketCliente.getInputStream()));
			paraCliente = new DataOutputStream(socketCliente.getOutputStream());
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void notificar(String mensagem){
		try{
			paraCliente.writeBytes(mensagem);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	@Override
	public void run(){
		clienteThread = servidor.getServidores();
		filaThread = new ArrayList<ClienteThread>(2);
		visao = new Visao(doCliente, paraCliente, servidor);
		regra = new Regra();
		
		try {

			nomeJogador = visao.obterNome();

			if(nomeJogador.equals(clienteThread.get(0).nomeJogador)){
				clienteThread.get(0).caractereJogo = 'X';
			}
			else if(nomeJogador.equals(clienteThread.get(1).nomeJogador)){
				clienteThread.get(1).caractereJogo = 'O';
			}
			
			
			iniciarMenu();
				
			filaThread.add(this);
				
			filaThread.get(0).iniciarJogo();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private synchronized void iniciarMenu() throws IOException, InterruptedException {	
		String comando = " ";
		visao.mostrarMsgJogoVelha();
		
		while(true){
			if(comando.equals("ok")){
				comando = " ";
				break;
			}
			else{
				visao.mostrarDeixa();
				comando = doCliente.readLine();
			}
			
			if(comando.equals("ajuda")){
				visao.mostrarAjuda();
			}
			else if(comando.equals("iniciar") && clienteThread.size() >= 2){
				if(this.nomeJogador.equals(clienteThread.get(0).nomeJogador)){
					clienteThread.get(1).visao.mostrarMsgAceitarJogo();
					comando = clienteThread.get(1).doCliente.readLine();
					break;
				}
				else if(this.nomeJogador.equals(clienteThread.get(1).nomeJogador)){
					clienteThread.get(0).visao.mostrarMsgAceitarJogo();
					comando = clienteThread.get(0).doCliente.readLine();
					break;
				}
			}
			else if(comando.equals("iniciar") && clienteThread.size() < 2){
				visao.mostrarMsgAguardeJogador();
			}
			else if(comando.equals("listar")){
				visao.listarJogadores();
			}
			else if(comando.equals("sair")){
				visao.mostrarMsgDesconectado();
				this.paraCliente.close();
				this.doCliente.close();
				for(ClienteThread clienteT: clienteThread){
					if(clienteT.nomeJogador.equals(this.nomeJogador)){
						clienteThread.remove(clienteT);
						break;
					}
				}
			}
			else if(comando.equals("ok")){
				break;
			}
			else{
				visao.mostrarMsgComandoErrado();
			}
		}
	}
	
	private void limparConsole() throws IOException {
		for(ClienteThread servidor: clienteThread){
			servidor.visao.limparTela();
		}
	}

	
	private synchronized void iniciarJogo() throws IOException, InterruptedException {
		servidor.iniciar();
		regra.inicializarMatriz();
		
		int jogadorDoisJogadas = 4;
		int jogadorUmJogadas = 5;
		
		visao.mostrarMsgJogaCom();
		mostrarMatriz();
		
		while(clienteThread.size() == 2){			
			if(regra.checar() == 'X'){ //jogador 1 vence
				limparConsole();
				mostrarMatriz();
				visao.mostrarMsgVitoriaJogador1();
				
				break;
			}
			else if(regra.checar() == 'O'){ //jogador 2 vence
				limparConsole();
				mostrarMatriz();
				visao.mostrarMsgVitoriaJogador2();
				
				break;
			}
			else if(jogadorUmJogadas == 0){
				visao.mostrarMsgEmpatou();
				
				break;
			}
			else if(regra.checar() == ' '){ //o jogo continua pois ninguem venceu ainda
				if(jogadorUmJogadas > 0){
					regra.setMatriz(clienteThread.get(0).visao.obterJogada(regra.getMatriz(), 'X'));
					jogadorUmJogadas--;
					limparConsole();
					mostrarMatriz();
				}
				if(regra.checar() == 'X'){
					continue;
				}
				if(jogadorDoisJogadas > 0){
					regra.setMatriz(clienteThread.get(1).visao.obterJogada(regra.getMatriz(), 'O'));
					jogadorDoisJogadas--;
					limparConsole();
					mostrarMatriz();
				}
				if(regra.checar() == 'O'){
					continue;
				}
			}
		}
		if(this.nomeJogador.equals(clienteThread.get(0).nomeJogador) && regra.checar() == 'X'){
			clienteThread.get(1).socketCliente.close();
		}
		else if(this.nomeJogador.equals(clienteThread.get(1).nomeJogador) && regra.checar() == 'O'){
			clienteThread.get(0).socketCliente.close();
		}

		iniciarMenu();
		
	}

	private void mostrarMatriz() throws IOException {
		for(ClienteThread servidor: clienteThread){
			servidor.visao.mostrarMatriz(regra.getMatriz());
		}
	}
}
