package br.ufg.inf.servidor;

public class Regra {
	private static char[][] matriz = new char[3][3];
	
	public void inicializarMatriz(){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				matriz[i][j] = ' ';
			}
		}
	}

	public char[][] getMatriz() {
		return matriz;
	}

	public void setMatriz(char[][] matriz) {
		Regra.matriz = matriz;
	}
	
	public synchronized char checar(){
		int i;
	    
	    for (i = 0; i < 3; i++) {
	        if ((matriz[i][0] == matriz[i][1]) && (matriz[i][0] == matriz[i][2])) { //verifica as 3 linhas a procura de sequencia igual
	            return matriz[i][0];
	        }
	    }
	    
	    for (i = 0; i < 3; i++) {
	        if ((matriz[0][i] == matriz[1][i]) && (matriz[0][i] == matriz[2][i])) { //verifica as 3 colunas
	            return matriz[0][i];
	        }
	    }
	    
	    for (i = 0; i < 3; i++) {
	        if ((matriz[0][0] == matriz[1][1]) && (matriz[1][1] == matriz[2][2])) { //verifica a diagonal principal
	            return matriz[0][0];
	        }
	    }
	    
	    for (i = 0; i < 3; i++) {
	        if ((matriz[0][2] == matriz[1][1]) && (matriz[1][1] == matriz[2][0])) { //verifica a diagonal secundária
	            return matriz[0][2];
	        }
	    }

		return ' ';
	}
	
	public boolean verificarSeJaMarcada(int linha, int coluna){
		boolean marcada = false;
		
		if(matriz[linha][coluna] != ' '){
			marcada = true;
		}
		
		return marcada;
	}
}
