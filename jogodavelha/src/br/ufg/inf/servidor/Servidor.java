package br.ufg.inf.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {
	private Visao visao = new Visao(null, null, this);
	private static final int PORTA = 7755;
	ArrayList<ClienteThread> threadServidor = new ArrayList<ClienteThread>();
	ServerSocket socketServidor;
	
	int status = 0;
		
	public Servidor(int porta){
		try{
			this.socketServidor = new ServerSocket(porta);
			visao.mostrarMsgAguardandoConex(porta);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String args[]) throws IOException{
		Servidor servidorA = new Servidor(PORTA); //se cria
		
		while(true){
			if(servidorA.getStatus() == 0 && servidorA.threadServidor.size() < 2){ //se jogo ainda não começou e não tem 2 jogadores conectados
				Socket socketCliente = servidorA.getConexaoServidor().accept();
				System.out.println("Novo cliente conectado:\nIP "+ socketCliente.getInetAddress().getHostAddress() +"\n"
						+ "Host name "+ socketCliente.getInetAddress().getHostName() +"\nPorta "+ socketCliente.getPort());
				ClienteThread threadServidor = new ClienteThread(socketCliente, servidorA); //thread criada
				
				servidorA.getServidores().add(threadServidor); //thread adicionada na lista
				threadServidor.start(); //thread inicializada
			}
		}
	}
	
	ArrayList<ClienteThread> getServidores(){
		return threadServidor;
	}
	
	ServerSocket getConexaoServidor(){
		return socketServidor;
	}
	
	int getStatus(){
		return status;
	}
	
	void iniciar() throws IOException{
		status = 1;
		visao.mostrarMsgJogoComecou();
	}
}
