/**
 * 
 */
package br.ufg.inf.servidor;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author weryquessantos
 *
 */
public class Visao {
	private Servidor servidor;
	private ArrayList<ClienteThread> threadServidor;
	private BufferedReader doCliente;
	private DataOutputStream paraCliente;
	private Regra regra = new Regra();

	public Visao(BufferedReader doCliente, DataOutputStream paraCliente, Servidor servidor) {
		this.doCliente = doCliente;
		this.paraCliente = paraCliente;
		this.servidor = servidor;
	}

	public void mostrarDeixa() throws IOException {
		paraCliente.writeChars(">_ ");
	}

	public void limparTela() throws IOException {
		for (int i = 0; i < 100; i++) {
			paraCliente.writeChars("\r\n");
		}
	}

	public void mostrarAjuda() throws IOException {
		paraCliente.writeChars(
				"\r\n"
				+ "+------------------------------------------------------------+\r\n"
				+ "| ajuda     mostra ajuda/intrucoes do jogo.                  |\r\n"
				+ "| iniciar   inicia o jogo caso tenha 2 jogadores conectados. |\r\n"
				+ "| listar    mostra jogadores conectados.                     |\r\n"
				+ "| sair      desconecta do servidor.                          |\r\n"
				+ "+------------------------------------------------------------+"
				+ "\r\n\n");
	}

	public void mostrarMsgJogoVelha() throws IOException {
		paraCliente.writeChars("JOGO DA VELHA. Digite ajuda (para instrucoes).\r\n\n");
	}

	public void mostrarMsgEmpatou() throws IOException {
		threadServidor = servidor.getServidores();
		for (ClienteThread servidor : threadServidor) {
			servidor.paraCliente.writeChars("Empatou!\r\n\n");
		}
	}

	public void mostrarMsgVitoriaJogador1() throws IOException {
		threadServidor = servidor.getServidores();
		threadServidor.get(0).paraCliente.writeChars("Voce venceu!\r\n\n");
		threadServidor.get(1).paraCliente.writeChars("Voce perdeu!\r\n\n");
	}

	public void mostrarMsgVitoriaJogador2() throws IOException {
		threadServidor = servidor.getServidores();
		threadServidor.get(1).paraCliente.writeChars("Voce venceu!\r\n\n");
		threadServidor.get(0).paraCliente.writeChars("Voce perdeu!\r\n\n");
	}

	public void mostrarMsgAguardeJogador() throws IOException {
		paraCliente.writeChars("Aguarde mais um jogador se conectar!\r\n\n");
	}

	public void mostrarMsgComandoErrado() throws IOException {
		paraCliente.writeChars("Comando nao encontrado.\r\n\n");
	}

	public void mostrarMsgDesconectado() throws IOException {
		paraCliente.writeChars("Desconectado\r\n\n");
	}

	public void mostrarMsgAceitarJogo() throws IOException {
		paraCliente.writeChars("Digite ok para aceitar o pedido de jogo: \r\n");
	}

	public void mostrarMsgJogaCom() throws IOException {
		threadServidor = servidor.getServidores();
		threadServidor.get(0).paraCliente.writeChars("Voce joga com X.\r\n\n");
		threadServidor.get(1).paraCliente.writeChars("Voce joga com O.\r\n\n");
	}

	public void mostrarMsgAguardandoConex(int porta) {
		System.out.println("Esperando conexoes na porta: " + porta);
	}

	public void mostrarMsgJogoComecou() {
		threadServidor = servidor.getServidores();
		for (ClienteThread servidor : threadServidor) {
			servidor.notificar("\r\nO jogo comecou!\r\n\n");
		}
	}

	public void mostrarMatriz(char[][] matriz) throws IOException {
		String matrizExibida = null;

		matrizExibida = "\r\n";
		for (int i = 0; i < 3; i++) {
			matrizExibida += " " + matriz[i][0] + " | " + matriz[i][1] + " | " + matriz[i][2] + " ";

			if (i != 2) {
				matrizExibida += "\r\n---|---|---\r\n";
			}
		}
		matrizExibida += "\r\n";
		paraCliente.writeChars(matrizExibida);
		paraCliente.flush();
	}

	public char[][] obterJogada(char[][] matriz, char caractereJogo) throws IOException {
		int linha = 0;
		int coluna = 0;

		while (true) {
			try{
				paraCliente.writeChars("Digite as coordenadas.\r\nDigite a linha: ");
				paraCliente.flush();
				linha = Integer.parseInt(doCliente.readLine());

				paraCliente.writeChars("\r\nDigite a coluna: ");
				paraCliente.flush();
				coluna = Integer.parseInt(doCliente.readLine());

				if (regra.verificarSeJaMarcada(linha, coluna)) {
					paraCliente.writeChars("A posicao ja foi preenchida, digite outra.\r\n\n");
					paraCliente.flush();
				} else {
					matriz[linha][coluna] = caractereJogo;
					break;
				}
			}catch(Exception e){
				paraCliente.writeChars("\r\nErro! Tente de novo.\r\n");
				e.printStackTrace();
			}
		}

		return matriz;
	}

	public String obterNome() throws IOException {
		String nomeJogador = null;

		paraCliente.writeChars("Forneca seu nome: ");
		paraCliente.flush();

		nomeJogador = doCliente.readLine();

		paraCliente.writeChars("Ola, " + nomeJogador + "!\r\n");
		paraCliente.flush();

		return nomeJogador;
	}

	public void listarJogadores() throws IOException {
		String jogadores = null;
		
		threadServidor = servidor.getServidores();
		jogadores = "\r\n+--------------------------+";
		jogadores += "\r\n| "+ threadServidor.size() +" Jogador(es).\r\n";
		for (ClienteThread servidor : threadServidor) {
			jogadores += "| Nome: " + servidor.nomeJogador + "\r\n";
		}
		jogadores += "+--------------------------+\r\n";
		paraCliente.writeChars(jogadores);
		paraCliente.flush();
	}
}
