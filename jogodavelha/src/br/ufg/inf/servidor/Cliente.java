package br.ufg.inf.servidor;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Cliente {
    public static void main(String[]args) throws IOException{ 
    Scanner s = new Scanner(System.in);
    String ip = null;
    int porta; 
    System.out.println("1 - Entrar no jogo");
    System.out.println("0 - Sair");
    int i = 1;
    while(true){
    System.out.println("Escolha uma opção: ");
    i = s.nextInt();
    if(i==1){
        try{
        System.out.println("Insira o IP: ");
        ip = s.next();
        System.out.println("Insira a porta: ");
        porta = s.nextInt();
        Socket cliente = new Socket(ip, porta);
    }
        catch(UnknownHostException e) {
        System.out.println("O endereço é inválido");
        e.printStackTrace();
    }
    catch(IOException e) {
        System.out.println("O servidor está fora do ar");
        e.printStackTrace();
    }
    }
    if(i==0) {
        System.exit(0);
    }
    }
    }
}
    